#include <SDL.h>
#include <iostream>
#include "sphere.h"
#include "hitable_list.h"
#include "float.h"
#include "camera.h"
#include "random.h"
#include "material.h"

#define WIN_WIDTH 1000
#define WIN_HEIGHT 700
#define WIN_DEPTH 10


vec3 color(const ray& r, hitable *world, int depth) 
{
	hit_record rec;
	if (world->hit(r, 0.001f, FLT_MAX, rec)) 
	{
		ray scattered;
		vec3 attenuation;
		if (depth < 50 && rec.mat_ptr->scatter(r, rec, attenuation, scattered)) 
		{
			return attenuation * color(scattered, world, depth + 1);
		}
		else
		{
			return vec3();
		}
	}
	else
	{
		vec3 unit_direction = unit_vector(r.direction());
		float t = 0.5 * (unit_direction.y() + 1.0);
		return (1.0 - t) * vec3(1.0, 1.0, 1.0) + t * vec3(0.5, 0.7, 1.0);
	}
}

hitable* random_scene() 
{
	int n = 500;
	hitable** list = new hitable * [n + 1];
	list[0] = new sphere(vec3(0, -1000, 0), 1000, new lambertian(vec3(0.5, 0.5, 0.5)));
	int i = 1;
	for (int a = -11; a < 11; a++) 
	{
		for (int b = -11; b < 11; b++) 
		{
			float choose_mat = random::rand_float(0.0, 1.0);
			vec3 center(a + 0.9 * random::rand_float(0.0, 1.0), 0.2, b + 0.9 * random::rand_float(0.0, 1.0));
			if ((center - vec3(4, 0.2, 0)).length() > 0.9) 
			{
				if (choose_mat < 0.8) 
				{  // diffuse
					list[i++] = new sphere(
						center, 0.2,
						new lambertian(vec3(random::rand_float(0.0, 1.0) * random::rand_float(0.0, 1.0),
							random::rand_float(0.0, 1.0) * random::rand_float(0.0, 1.0),
							random::rand_float(0.0, 1.0) * random::rand_float(0.0, 1.0)))
					);
				}
				else if (choose_mat < 0.95) 
				{ // metal
					list[i++] = new sphere(
						center, 0.2,
						new metal(vec3(0.5 * (1 + random::rand_float(0.0, 1.0)),
							0.5 * (1 + random::rand_float(0.0, 1.0)),
							0.5 * (1 + random::rand_float(0.0, 1.0))),
							0.5 * random::rand_float(0.0, 1.0))
					);
				}
				else 
				{  // glass
					list[i++] = new sphere(center, 0.2, new dielectric(1.5));
				}
			}
		}
	}

	list[i++] = new sphere(vec3(0, 1, 0), 1.0, new dielectric(1.5));
	list[i++] = new sphere(vec3(-4, 1, 0), 1.0, new lambertian(vec3(0.4, 0.2, 0.1)));
	list[i++] = new sphere(vec3(4, 1, 0), 1.0, new metal(vec3(0.7, 0.6, 0.5), 0.0));

	return new hitable_list(list, i);
}

void main_viewport(char * pixels)
{
	char* tmp = pixels;
	int nx = WIN_WIDTH;
	int ny = WIN_HEIGHT;
	int ns = WIN_DEPTH;
	hitable* list[5];
	list[0] = new sphere(vec3(0, 0, -1), 0.5, new lambertian(vec3(0.1, 0.3, 0.3)));
	//  this is a second, much larget sphere, which appears to be green
	list[1] = new sphere(vec3(0, -100.5, -1), 100, new lambertian(vec3(0.8, 0.8, 0.0)));
	list[2] = new sphere(vec3(1,0,-1), 0.5, new metal(vec3(0.8, 0.6, 0.2), 0.3));
	list[3] = new sphere(vec3(-1, 0, -1), 0.5, new dielectric(1.5));
	list[4] = new sphere(vec3(-1, 0, -1), -0.45, new dielectric(1.5));
	hitable* world = new hitable_list(list, 5);
	//hitable* world = random_scene();

	camera cam(vec3(-2, 2, 1), vec3(0, 0, -1), vec3(0, 1, 0), 50, float(nx) / float(ny));
	//camera cam(vec3(13,2,3), vec3(0, 0, 0), vec3(0, 1, 0), 20, float(nx) / float(ny));

	for (int j = ny - 1; j >= 0; j--)
	{
		for (int i = 0; i < nx; i++)
		{
			vec3 col;
			for (int s = 0; s < ns; s++)
			{
				float u = float(i + random::rand_float(0.0, 1.0)) / float(nx);
				float v = float(j + random::rand_float(0.0, 1.0)) / float(ny);
				ray r(cam.get_ray(u, v));
				col += color(r, world, 0);
			}
			col /= float(ns);
			col = vec3(sqrt(col[0]), sqrt(col[1]), sqrt(col[2])); 
			uint8_t ir = uint8_t(255.99 * col[0]);
			uint8_t ig = uint8_t(255.99 * col[1]);
			uint8_t ib = uint8_t(255.99 * col[2]);
			tmp[0] = ir;
			tmp[1] = ig;
			tmp[2] = ib;
			tmp += 3;
		}
	}
}

int sdl_stuff()
{
	random::initialize();

	std::cout << "success\n";
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		std::cout << "Failed to initialize the SDL2 library\n";
		return -1;
	}

	SDL_Window* window = SDL_CreateWindow("SDL2 Window",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		WIN_WIDTH, WIN_HEIGHT,
		0);

	if (!window)
	{
		std::cout << "Failed to create window\n";
		return -1;
	}

	//SDL_Surface* window_surface = SDL_GetWindowSurface(window);
	//screen texture streaming
	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	SDL_Texture* screen_texture = SDL_CreateTexture(renderer,
		SDL_PIXELFORMAT_RGB24, SDL_TEXTUREACCESS_STREAMING,
		WIN_WIDTH, WIN_HEIGHT);
	//screan texture streaming

	int channels = 3; // for a RGB image
	char* pixels = new char[WIN_WIDTH * WIN_HEIGHT * channels];

	main_viewport(pixels);

	// populate pixels with real data ...

	//SDL_Surface *pixel_surface = SDL_CreateRGBSurfaceFrom((void*)pixels,
 //               WIN_WIDTH,
 //               WIN_HEIGHT,
 //               channels * 8,          // bits per pixel = 24
 //               WIN_WIDTH * channels,  // pitch
 //               0x0000FF,              // red mask
 //               0x00FF00,              // green mask
 //               0xFF0000,              // blue mask
 //               0); 


	//if (!window_surface)
	//{
	//	std::cout << "Failed to get the surface from the window\n";
	//	return -1;
	//}

	bool keep_window_open = true;
	Uint64 start, now;
	while (keep_window_open)
	{
		SDL_Event e;

		while (SDL_PollEvent(&e) > 0)
		{
			start = SDL_GetPerformanceCounter();
			switch (e.type)
			{

			case SDL_KEYDOWN:

				switch (e.key.keysym.sym)
				{

				case SDLK_ESCAPE:
					keep_window_open = false;
					break;

				}

				break;
			case SDL_QUIT:
				keep_window_open = false;
				break;
			}

			//SDL_BlitSurface(pixel_surface, NULL, window_surface, NULL);
			//SDL_UpdateWindowSurface(window);
			SDL_UpdateTexture(screen_texture, nullptr, pixels, WIN_WIDTH * channels);
			SDL_RenderClear(renderer);
			SDL_RenderCopy(renderer, screen_texture, nullptr, nullptr);
			SDL_RenderPresent(renderer);
			now = SDL_GetPerformanceCounter();
			SDL_Log("main tick took: %f seconds", ((double)((now - start) * 1000) / SDL_GetPerformanceFrequency()) / 100);
		}
	}

	return 0;
}


int main(int ac, char** av)
{
	return sdl_stuff();
}

