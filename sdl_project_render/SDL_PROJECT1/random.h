#pragma once
#include <random>

struct random 
{
	static void initialize();
	static float rand_float(float a, float b);

private:
	static std::mt19937 gen;
};


