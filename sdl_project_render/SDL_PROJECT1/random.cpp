#include "random.h"

void random::initialize() 
{
	std::random_device rd;  //Will be used to obtain a seed for the random number engine
	std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
}

float random::rand_float(float a, float b)
{
	std::uniform_real_distribution<> dis(0, 1.0);
	return dis(gen);
}

std::mt19937 random::gen;